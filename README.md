# Drama Queen without queen 👑🙅‍♀
This is my first Snapchat lens. Basically this is like the `Drama Queen` lens from Snapchat but without the text on the cheeks.

## Why does it exist? 🧬
A friend of mine loves the `Drama Queen` lens but sometimes they want to post a image with it on social media. The only problem, they don't want the text. I used to help them and remove the text after the fact with Gimp, but I got tired and created this lens 😊

## How to get it 💾
Scan this Snapcode:

![https://t.snapchat.com/JQBOP488](Previews/snapcode.svg)

Or open this link: https://t.snapchat.com/JQBOP488

Or download [Lens Studio](https://ar.snap.com/download) and open this project.

## How does it look? 📸
(I used the image that comes combined with Lens Studio)

![Dull face with lens applied](Previews/face_2.png)
