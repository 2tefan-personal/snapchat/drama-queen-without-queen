//@input bool enableBlush = true {"label": "Blush"}
//@ui {"widget":"group_start", "label": "Blush Properties",  "showIf" : "enableBlush"}
//@input vec3 colorBlush = {0.92,0.65,0.68} {"widget":"color", "showIf" : "enableBlush", "label":"Color"}
//@input float alphaBlush = 0.5 {"widget":"slider", "min": 0.0, "max": 1.0, "step": 0.01, "showIf" : "enableBlush", "label": "Intensity"}
//@ui {"widget":"group_end"}
//@ui {"widget":"separator"}


function setColor(_component, _color, alpha) {
    var pass = _component.mainPass;
    if (pass) {
        pass.baseColor = new vec4(_color.x, _color.y, _color.z, alpha);
    } else {
        print("[MakeupController] ERROR: Material is not set for " + _component.getSceneObject().name);
    }
}

if (script.lips) {
    script.lips.enabled = script.enableLips;
    script.lips.faceIndex = script.faceIndex;
    
    var lipsController = script.lipsController;
    
    if (lipsController) { 
        if (script.applyLipsFix) {
            lipsController.enabled = true;
        } else {
            lipsController.enabled = false;
        }
    }
    setColor(script.lips, script.colorLips, script.alphaLips);
} else if (script.enableLips) {
    print("[MakeupController] ERROR: Lips object is not assigned or doesn't exist. Please assign it under Advanced checkbox");
}

if (script.lipgloss) {
    script.lipgloss.enabled = script.enableLipgloss;
    script.lipgloss.faceIndex = script.faceIndex;
    setColor(script.lipgloss, script.colorLipgloss, script.alphaLipgloss);
} else if (script.enableLipgloss) {
    print("[MakeupController] ERROR: Lipgloss object is not assigned or doesn't exist. Please assign it under Advanced checkbox");
}

if (script.blush) {
    script.blush.enabled = script.enableBlush;
    script.blush.faceIndex = script.faceIndex;
    setColor(script.blush, script.colorBlush, script.alphaBlush);
} else if (script.enableBlush) {
    print("[MakeupController] ERROR: Blush object is not assigned or doesn't exist. Please assign it under Advanced checkbox");
}
